import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.*;

import java.io.IOException;

public class LongestWordReducer  extends Reducer<Text,IntWritable,Text,IntWritable>
{
    private Text _longestWord = new Text();

    public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException
    {
        for (IntWritable val : values)	//проходим по всем входным наидлиннейшим в строках словам, и ищем самое длинное
	    {
            if (key.getLength() > _longestWord.getLength()) _longestWord.set(key);
        }
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException
    {
        context.write(_longestWord, new IntWritable(_longestWord.getLength()));	//после завершения прохода по всем словам, выводим итог
    }
}

