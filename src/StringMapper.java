import java.nio.charset.Charset;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Counter;

public class StringMapper extends Mapper<Object, Text, Text, IntWritable>
{
    private Text _word = new Text();
    private Text _longestWord = new Text();

    private static boolean isPureAscii(String stringToCheck)	//проверка на то, содержит ли входная строка только ASKII символы
    {
        return Charset.forName("US-ASCII").newEncoder().canEncode(stringToCheck);
    }

    public void map(Object key, Text value, Context context) throws IOException, InterruptedException
    {
        StringTokenizer words = new StringTokenizer(value.toString());	//разбиение строки на отдельные слова
        _longestWord.clear();
        Counter counterMalformedWords = context.getCounter("MyCounters","MalformedWords");
        while (words.hasMoreTokens())
        {
            _word.set(words.nextToken());
            if (isPureAscii(_word.toString()))	//проверка слова на ASKII символы
            {
                if (_word.getLength()>_longestWord.getLength()) _longestWord.set(_word);	//и на длину в сравнении с предыдущим наидлиннейшим словом
            }
            else
            {
                counterMalformedWords.increment(1);	//строка содержит не только символы ASKII, увеличиваем на вединицу счётчик неверно сформированных слов
            }
        }
        context.write(_longestWord, new IntWritable(0));
    }
}

